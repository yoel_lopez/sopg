#include "TP2.h"

int server_config()
{
	printf(SERVER_CONFIG);
    bzero((char*) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(SOCKET_PORT);
    serveraddr.sin_addr.s_addr = inet_addr(SOCKET_IP);
    if(serveraddr.sin_addr.s_addr==INADDR_NONE)
    {
       	perror(SERVER_ERROR);
       	return ERROR;
    }
	return OK;
}
