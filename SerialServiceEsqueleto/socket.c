#include "TP2.h"

int socket_create()
{
	printf(SOCKET_CREATE);
	if((id_socket = socket(AF_INET,SOCK_STREAM, 0)) == -1)
	{
		perror(SOCKET_OPEN_ERROR);
		return ERROR;
	}
	return OK;
}

int socket_close()
{
	if(close(id_socket)==-1){printf(SOCKET_CLOSE_ERROR);return ERROR;}
}


int socket_bind()
{
	printf(SERVER_OPEN);
	if (bind(id_socket, (struct sockaddr*)&serveraddr, sizeof(serveraddr)) == -1) {
		close(id_socket);
		perror(BIND_ERROR);
		return ERROR;
	}
	return OK;
}

int socket_accept()
{
	addr_len = sizeof(struct sockaddr_in);
    if((new_file_descriptor = accept(id_socket, (struct sockaddr *)&clientaddr, &addr_len)) == -1)
    {	
		perror(ACCEPT_ERROR);
		sleep(5);
		return ERROR;
	}
	return OK;
}

int socket_listen()
{
	printf(LISTEN_CONFIG);
	if(listen(id_socket, BACKLOG) == -1) // backlog=10
  	{
    	perror(LISTEN_ERROR);
    	return ERROR;
  	}
	return OK;
}

int socket_send()
{
	if(write(new_file_descriptor, buffer_ciaa , sizeof(buffer_ciaa)) == -1)
    {
    	perror(WRITING_ERROR);
    	return ERROR;
    }
	return OK;
}

int socket_receive()
{
	if((N_bytes_Socket = read(new_file_descriptor,buffer_socket,sizeof(buffer_socket))) == -1)
	{
		perror(READING_ERROR);
		return ERROR;
	}
	if(N_bytes_Socket ==0){
		perror(READING_NOBYTES);
		return ERROR;
	} 
	if (buffer_socket[0] == 0) {
		perror(READING_EOF);
		return ERROR;
	}
	buffer_socket[N_bytes_Socket]=END_OF_STRING;
	return OK;
}





