#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include "SerialManager.h"
#include "rs232.h"
#include "TP2.h"

void bloquearSign(void);
void desbloquearSign(void);
void sigint_handler(int sig);
void sigterm_handler(int sig);
void config_signals(void);


int SocketOK; //variable para saber si esta todo ok con el socket
pthread_t tid1;
int signalExit;

// Este thread consulta cada 100mseg la uart y lo reenvía por la conexión TCP
void *uart2tcp() {

	while (!signalExit) {
		if (N_bytes_CIAA = serial_receive(buffer_ciaa, BUFFER_SIZE)) {
			buffer_ciaa[N_bytes_CIAA] = END_OF_STRING;
			printf(CIAA_MSG, buffer_ciaa);
			if (SocketOK != 0) {
				// Enviamos mensaje a cliente
				if (socket_send()==OK) {
					printf(READING_MSG, N_bytes_Socket);
				} else
					printf(READING_MSG_ERROR);
			}
		}
		sleep(0.1);
	}
	printf("Llego signalExit=1\r\n");
}

int main() {

	int result;

	config_signals();
	bloquearSign();

	printf("Inicializamos la uart\r\n");
	if (serial_open(1, 115200)) {
		return ERROR;
	} // Abrir puerto serie

	printf("Inicio Serial Service\r\n");

	SocketOK = 0;

//se crea el socket
	printf("1 creando socket\r\n");
	if (socket_create()) {
		return ERROR;
	} // Crear socket
	printf("2 socket creado\r\n");

// Cargamos datos de IP:PORT del server
	if (server_config()) {
		return ERROR;
	} // Configurar IP:PORT del

// Abrimos puerto con bind()
	if (socket_bind()) {
		return ERROR;
	} // Abrir puerto con bind()

// Seteamos socket en modo Listening
	if (socket_listen()) {
		return ERROR;
	} // Configurar socket en modo Listening
	printf(SERVER_CONFIG_MSG);

	addr_len = sizeof(struct sockaddr_in);
	char ipClient[32];


	signalExit=0;
	pthread_create(&tid1, NULL, uart2tcp, NULL); //crea el hilo
	desbloquearSign();
	// Ejecutamos accept() para recibir conexiones entrantes


	while (!signalExit) {
		if (socket_accept() == ERROR) {
			continue;
		}
		printf("6 accept listo para recibir conexiones entrantes\r\n");

		SocketOK = 1;
		while (1) {
			if (socket_receive() == ERROR) {
				SocketOK = 0;
				break;
			}
			else
				serial_send(buffer_socket, sizeof(buffer_socket)); // Enviar el ACK a la CIAA
		}

		close(new_file_descriptor);
		printf("Saliendo...\r\n");

	}
	printf("Se presionó ctrl-c\r\n");
	serial_close();

	socket_close();

	pthread_join(tid1, NULL);
	printf("Terminó hilo secundario ctrl-c\r\n");
}
