#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "SerialManager.h"
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

typedef enum{OK,ERROR}status;

#define BUFFER_SIZE 10
#define ALWAYS 1
#define END_OF_STRING '\0'

/*############ SERIAL PORT #################*/
#define SERIAL_PORT 1
#define BAUD_RATE 115200
#define SERIAL_PORT_ERROR "Puerto serie: Error!"

/*############ SOCKET & SERVER #################*/

#define SOCKET_CREATE "Servidor> Creando socket\r\n"
#define SOCKET_CLOSE "Servidor> Cerrando socket\r\n"
#define SOCKET_OPEN_ERROR "Socket: Error de creacion!"
#define SERVER_CONFIG "Servidor> Configurando . . .\r\n"
#define SOCKET_PORT 10000
#define SOCKET_IP "127.0.0.1"
#define SERVER_ERROR "Servidor: Error, IP inválida\r\n"
#define SERVER_OPEN "Servidor> Abriendo puerto ...\r\n"
#define BIND_ERROR "Servidor: Error en bind()"
#define LISTEN_CONFIG "Servidor> Seteando modo Listening\r\n"
#define BACKLOG 10
#define LISTEN_ERROR "Servidor: Error en listen"
#define SERVER_CONFIG_MSG "Servidor> IP:%s\r\n", "127.0.0.1 : 10000"
#define ACCEPT_ERROR "Servidor: Error en accept"
#define CIAA_MSG "De la uart recibimos: %s"
#define INTERFACE_ACK ">OK"
#define INTERFACE_ACK_SIZE sizeof(INTERFACE_ACK)-1
#define WRITING_ERROR "Socket: Error escribiendo mensaje en socket\r\n"
#define READING_ERROR "Socket: Error leyendo mensaje en socket\r\n"
#define READING_EOF "Socket: Error recibe EOF\r\n"
#define READING_NOBYTES "Socket: Error recibe cero bytes\r\n"
#define READING_MSG "Serial: Recibi %d bytes\r\n"
#define READING_MSG_ERROR "Error escribiendo mensaje en socket\r\n"
#define SERIAL_MSG "Serial: %s\r\n"
#define SOCKET_CLOSE_ERROR "Socket: Error cerrando el socket\r\n"

/*############ PROTOTYPES #################*/
int serial_port_open();
int server_config();
int socket_create();
int socket_close();
int socket_bind();
int socket_listen();
int socket_accept();
int socket_send();
int socket_receive();

/*############ GLOBAL VARIABLES #################*/

int id_serial, id_socket, new_file_descriptor;
int N_bytes_CIAA, N_bytes_Socket;
char buffer_ciaa[BUFFER_SIZE];
char buffer_socket[BUFFER_SIZE];
socklen_t addr_len;
struct sockaddr_in clientaddr;
struct sockaddr_in serveraddr;
